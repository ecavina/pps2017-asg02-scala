case class Course(courseName: String, teacher: String)

trait Student {

  def courses: List[String]

  def hasTeacher(teacher: String): Boolean

}

object Student {
  def apply(name: String, courses: List[Course]): Student = StudentImpl(name, courses)

  case class StudentImpl(str: String, value: List[Course]) extends Student {

    override def courses: List[String] = {
      value map(x => x.courseName + ": " + x.teacher)
    }

    override def hasTeacher(teacher: String): Boolean = {
      value map(x => x.teacher) contains(teacher)
    }
  }
}

object TestCourse extends App {

  val student1 = Student("Eugenio", List(Course("italiano", "matulli"), Course("matematica", "lugatti")))

  println(student1.courses)
  println(student1.hasTeacher("matulli"))
  println(student1.hasTeacher("viroli"))

}

