/*
* SOME EXPERIMENTS WITH SCALA
* To run the worksheet: right click on file > evaluate worksheet
* */

import scala.annotation.tailrec
import scala.math.sqrt

// NESTED FUNCTION
// sqrt with Newton's approximations
def sqrt2(x: Double) :Double = {
  def sqrtIter(guess: Double): Double =
    if (isGoodEnough(guess)) guess
    else sqrtIter(improve(guess))

  def improve(guess: Double) =
    (guess + x / guess) / 2

  def isGoodEnough(guess: Double) =
    Math.abs(guess * guess - x) < 0.001

  sqrtIter(1.0)
}

// Tail recursive

def factorialNotTail(n: Int): Int =
  if (n == 0) 1 else n * factorial(n - 1)

def imperativeFactorial(n: Int): Int = {
  var result = 1
  var i = 1
  while (i <= n) {
    result = result * i
    i = i + 1
  }
  result
}

def factorial(n: Int): Int = {
  @tailrec
  def iter(x: Int, result: Int): Int =
    if (x == 0) result
    else iter(x - 1, result * x)

  iter(n, 1)
}

def exp(x: Int, n: Int): Int = {
  @tailrec
  def iter(number: Int, remaining: Int, result: Int):Int =
    if (remaining == 0) result
    else iter(number, remaining-1, result*number)

  iter(x, n, 1)
}

def fib(n: Int): Int = {
  @tailrec
  def iter(x: Int, first: Int, second: Int): Int =
    if (x==0) first
    else if (x==1) second
    else iter(x-1, second, first+second)

  iter(n, 0, 1)
}

def fib2(n: Int): Int = {
  @tailrec
  def iter(x: Int, first: Int, second: Int): Int =
    if (x==1) 1
    else if (x==2) 1
    else iter(x-1, x-first, x-second)

  iter(n, 0, 1)
}

def real(n: Int): Double = {
  @tailrec
  def iter(x: Double, result: Double): Double =
    if (x==0) result
    else iter(x-1, result+1.0)

  iter(n, 0.0)
}

// Traits and case classes

sealed trait NoteName
case object A extends NoteName
case object C extends NoteName

sealed trait Symbol
case class Note(name: NoteName, duration: String, octave: Int) extends Symbol
case class Rest(duration: String) extends Symbol

val symbol1: Symbol = Note(C, "Quarter", 3);
val symbol2: Symbol = Rest("Whole");

def symbolDuration(symbol: Symbol): String =
  symbol match {
    case Note(name, duration, octave) => duration
    case Rest(duration) => duration
  }

sealed trait Duration
case object Whole extends Duration
case object Half extends Duration
case object Quarter extends Duration

def fractionOfWhole(duration: Duration): Double =
  duration match {
    case Whole => 1.0
    case Half => 0.5
    case Quarter => 0.25
  }

//HIGH ORDER FUNCTION
def sumInts(a: Int, b: Int): Int =
  if (a > b) 0 else a + sumInts(a + 1, b)

def cube(x: Int): Int = x * x * x

def sumCubes(a: Int, b: Int): Int =
  if (a > b) 0 else cube(a) + sumCubes(a + 1, b)

def sumFactorials(a: Int, b: Int): Int =
  if (a > b) 0 else factorial(a) + sumFactorials(a + 1, b)

//common pattern
def sum(f: Int => Int, a: Int, b: Int): Int =
  if(a > b) 0
  else f(a) + sum(f, a+1, b)

def id(x: Int): Int = x
def sumInts2(a: Int, b: Int) = sum(id, a, b)
def sumCubes2(a: Int, b: Int) = sum(x => x * x * x, a, b)
def sumFactorials2(a: Int, b: Int) = sum(factorial, a, b)

//like last commons sum but with tail recursive
def sumTail(f: Int => Int, a: Int, b: Int): Int = {
  @tailrec
  def loop(x: Int, acc: Int): Int =
    if(x > b) acc
    else loop(x + 1, acc + f(x))

  loop(a, 0)
}

//Experiment with standard scala List
val cond: (Int, Int) => Boolean = (x, y) => if(x > y) false else true
def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => x :: Nil
    case y :: ys =>
      if (cond(x, y)) x :: y :: ys
      else y :: insert(x, ys)
  }

def insertionSort(xs: List[Int]): List[Int] = xs match {
  case List() => List()
  case y :: ys => insert(y, insertionSort(ys))
}

insertionSort(2 :: 1 :: 3 :: Nil)

//Experiment with standard scala Option
// The `sqrt` function is not defined for negative values
def sqrt3(x: Double): Option[Double] =
  if(x < 0) None else Some(sqrt(x))

def sqrt4(x: Double): String =
  sqrt3(x) match {
    case None => "no result"
    case Some(y) => y.toString
  }

//Experiment with standard scala Either
//Left e Right, right based so map and flatmap work on right
def triple(x: Int): Int = 3 * x

def tripleEither(x: Either[String, Int]): Either[String, Int] =
  x.right.map(triple)

tripleEither(Right(3))
tripleEither(Left("empty"))

//Standard Scala Stream
var rec = 0
def streamRange(lo: Int, hi: Int): Stream[Int] = {
  rec = rec + 1
  if (lo >= hi) Stream.empty
  else Stream.cons(lo, streamRange(lo + 1, hi))
}
streamRange(1, 10).take(3).toList
rec == 3

//Object Oriented Example
class Rational(x: Int, y: Int) {
  require(y > 0, "denominator must be positive") // precondition

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  private val g = gcd(x, y)

  def numer = x/g

  def denom = y/g

  //it is possible to change this methods name in operators, like "+" "-" "*" ...
  def add(r: Rational) =
    new Rational(numer * r.denom + r.numer * denom, r.denom * denom)

  def mul(r: Rational) =
    new Rational(numer * r.numer, denom * r.denom)

  def equal(r: Rational) =
    (numer == r.numer) && (denom == r.denom)

  def less(that: Rational) =
    this.numer * that.denom < that.numer * this.denom

  def max(that: Rational) =
    if (this.less(that)) that else this

  override def toString: String = numer + "/" + denom
}

val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)
x.add(y).mul(z)

def addRational(r: Rational, s: Rational): Rational =
 new Rational(
   r.numer * s.denom + s.numer * r.denom,
   r.denom * s.denom
 )

def makeString(r: Rational) =
  r.numer + "/" + r.denom

makeString(addRational(new Rational(1,2), new Rational(1,2)))

//Experiment with abstract classes
abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
}

class Empty extends IntSet {
  def contains(x: Int): Boolean = false
  def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {

  def contains(x: Int): Boolean =
    if (x < elem) left contains x
    else if (x > elem) right contains x
    else true

  def incl(x: Int): IntSet =
    if (x < elem) new NonEmpty(elem, left incl x, right)
    else if (x > elem) new NonEmpty(elem, left, right incl x)
    else this
}
