import scala.annotation.tailrec

//LAB 03


def compose(f: Int => Int, g: Int => Int)(i: Int): Int = f(g(i))

//---

def fib(n: Int): Int = n match {
  case 0 => 0
  case 1 | 2 => 1
  case _ => fib(n-1) + fib(n-2)
}

//---

def tailFib(n: Int): Int = {

  @tailrec
  def loop(index: Int, prev: Int, current: Int): Int = index match {
    case 0 => current
    case _ => loop(index-1, prev + current, prev)
  }

  loop(n, 1, 0)
}

//---

sealed trait Person
case class Teacher(name: String, course: String) extends Person
case class Student(name: String , year: Int) extends Person

def personToString(p: Person):String = p match {
  case Teacher(name, course) => name + ": " + course
  case Student(name, year) => name + ": " + year
}
//val p1: Person = Teacher("Matulli", "Italiano")
//val p2: Person = Student("Eugenio", 1994)
//
//println(personToString(p1))
//println(personToString(p2));

//---

// A generic linkedlist
sealed trait List[E]

// a companion object (i.e., module) for List
object List {
  case class Cons[E](head: E, tail: List[E]) extends List[E]
  case class Nil[E]() extends List[E]

  def length[E](l: List[E]): Int = l match {
    case Cons(h, t) => 1 + length(t)
    case _ => 0
  }

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] = (l1, l2) match {
    case (Cons(h, t), l2) => Cons[C](h, append(t, l2))
    case (l1, Cons(h, t)) => Cons[C](h, append(l1, t))
    case _ => Nil()
  }

  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(h, t), 0) => Cons(h, t)
    case (Cons(h, t), toDrop) => drop(t, toDrop-1)
    case _ => Nil()
  }

  def map[A,B](l: List[A])(f: A => B): List[B] = l match {
    case Cons(h, t) => Cons(f(h), map(t)(f))
    case _ => Nil()
  }

  def filter[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case Cons(h, t) => if(f(h)) Cons(h, filter(t)(f)) else filter(t)(f)
    case _ => Nil()
  }
}

//---

// An Optional data type
sealed trait Option[A]

object Option {
  case class None[A]() extends Option[A]
  case class Some[A](a: A) extends Option[A]

  def isEmpty[A](opt: Option[A]): Boolean = opt != None()

  def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  def filter[A](opt: Option[A])(pred: A => Boolean): Option[A] = opt match {
    case Some(x) => if(pred(x)) Some(x) else None()
    case None() => None()
  }

  def map[A,B](opt: Option[A])(pred: A => B): Option[B] = opt match {
    case Some(x) => Some(pred(x))
    case None() => None()
  }

  def map2[A](opt1: Option[A], opt2: Option[A])(combines: (A, A) => A): Option[A] = (opt1, opt2) match {
    case (Some(x), Some(y)) => Some(combines(x, y))
    case _ => None()
  }
}

def max(l: List[Int]): Option[Int] = {
  @tailrec
  def loop(l: List[Int], max: Option[Int]): Option[Int] = l match {
    case List.Cons(h, t) => if(h > Option.getOrElse(max, 0)) loop(t, Option.Some(h)) else loop(t, max)
    case List.Nil() => max
  }

  loop(l, Option.None())
}

//max(Cons(10, Cons(25, Cons(20, Nil())))) // Some(25)
//max(Nil()) // None()

//---

import List._
def getCoursesFromPersons(l: List[Person]): List[String] = {
  List.map(
    List.filter(l) { case Teacher(n, i) => true; case _ => false }
  ) ({ case Teacher(n, i) => i; case _ => "" })
}

getCoursesFromPersons(Cons(Teacher("Viroli", "OOP"),
  Cons(Student("Eugenio", 1000), Cons(Teacher("Ricci", "PCD"), Nil()))))

//---

//import Option._
//Option.filter(Some(5))(_ > 2) // Some(5)
//Option.filter(Some(5))(_ > 8) // None
//
//Option.map(Some(5))(_ > 2) // Some(true)
//Option.map(None[Int])(_ > 2) // None
//Option.map(Some(5))(_ + 2) // Some(7)
//
//Option.map2(Some(1), Some(2))((x: Int, y: Int) => x + y)
//Option.map2(None(), Some(2))((x: Int, y: Int) => x + y)
//Option.map2(Some("ciao"), Some("ciaone"))((x: String, y: String) => x + y)

//---

def foldRight[A,B](l: List[A])(initValue: B)(op: (A,B) => B): B = l match {
  case List.Cons(h, t) => foldRight(t)(op(h,initValue))(op)
  case _ => initValue
}

def foldLeft[A,B](l: List[A])(initValue: B)(op: (A,B) => B): B = l match {
  case List.Cons(h, List.Nil()) => op(h, initValue)
  case List.Cons(h, t) => op( h, foldLeft(t)(initValue)(op) )
  case _ => initValue
}

val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
foldRight(lst)(0)(_+_) // 16
foldRight(lst)("")(_+_) // "5173"
foldLeft(lst)("")(_+_) // "3715"

//---

sealed trait Either[L,R]

object Either {
  case class Left[L,R](l: L) extends Either[L,R]
  case class Right[L,R](r: R) extends Either[L,R]

  def left[L,R](el: Either[L,R]): L = el match {
    case Left(l) => l
    case _ => throw new NoSuchElementException("Either.left on Right")
  }

  def right[L,R](el: Either[L,R]): R = el match {
    case Right(r) => r
    case _ => throw new NoSuchElementException("Either.right on Left")
  }
}



