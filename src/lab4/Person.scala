package lab4


// A person with immutable name/surname, mutable married flag, and toString
trait Person {

  def name: String
  def surname: String
  def married: Boolean
  def married_=(state: Boolean): Unit

  // a template method
  override def toString(): String = name + " " + surname + " " + married
}

// easiest implementation, with overriding class parameters becoming fields
class PersonImpl1(override val name: String,
                  override val surname: String,
                  override var married: Boolean) extends Person

// Note uniform access
object UsePerson extends App {

  val p: Person = new PersonImpl1("mario", "rossi", false)
  println(p)

}